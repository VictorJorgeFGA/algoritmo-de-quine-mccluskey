#include "Simplificador.hpp"
#include <iostream>
#include <set>

using namespace std;

Simplificador::Simplificador( const vector<Termo> & primos, const vector<int> & mintermos ){
    this->primos = primos;
    this->mintermos = mintermos;
    montarMatriz();
    simplificar();
}

void Simplificador::montarMatriz(){
    sort( mintermos.begin() , mintermos.end() );
    matriz.resize( primos.size() );
    for( int i = 0 ; (unsigned long) i < matriz.size() ; i++ )
        matriz[i].resize( mintermos.size() , false );

    for( int i = 0 ; (unsigned long) i < matriz.size() ; i++ )
        for( int j = 0 ; (unsigned long) j < matriz[i].size() ; j++ )
            if( primos[i].cobreMintermo( mintermos[j] ) )
                matriz[i][j] = true;
}

void Simplificador::simplificar(){
    while( 1 ){
        int primo = getTermoEssencial();
        primo = primo == -1 ? getTermoRico() : primo;

        if( primo == -1 )   break;

        primosImplicantes.push_back( primos[primo] );
        cobrirMintermos( primo );
    }
}

int Simplificador::getTermoEssencial(){
    for( int j = 0 ; (unsigned long) j < mintermos.size() ; j++ ){
        int sum = 0, ultimo = -1;
        for( int i = 0 ; (unsigned long) i < matriz.size() ; i++ ){
            if( matriz[i][j] ){
                sum++;
                ultimo = i;
            }
        }
        if( sum == 1 )
            return ultimo;
    }
    return -1;
}

int Simplificador::getTermoRico(){
    int maior = 0;
    int dententor = -1;
    for(int i = 0 ; (unsigned long) i < matriz.size() ; i++){
        int atual = 0;
        for(int j = 0 ; (unsigned long) j < matriz[i].size() ; j++){
            if( matriz[i][j] ) atual++;
        }
        if( atual > maior ){
            maior = atual;
            dententor = i;
        }
    }
    return dententor;
}

void Simplificador::cobrirMintermos( int primo ){
    for( int k = 0 ; (unsigned long) k < mintermos.size() ; k++)
        if( matriz[primo][k] )
            for( int i = 0 ; (unsigned long) i < matriz.size() ; i++ )  
                matriz[i][k] = false;
}

void Simplificador::exibirPrimosImplicantes(){
    for( int i = 0 ; (unsigned long) i <  primosImplicantes.size() ; i++ ){
        primosImplicantes[i].exibir();
        cout << endl;
    }
}

const vector<Termo> & Simplificador::getPrimosImplicantes() const{
    return primosImplicantes;
}