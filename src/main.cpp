#include <bits/stdc++.h>
#include "mccluskey.hpp"

using namespace std;

int main(){
    vector<vector<int>> tabelaVerdade;
    int n,m;
    cin >> m >> n;
    for( int i = 0 ; i < m ; i++ ){
        vector<int> temp;
        for( int j = 0 ; j < n ; j++ ){
            int x;  cin >> x;
            temp.push_back(x);
        }
        tabelaVerdade.push_back( temp );
    }
    McCluskey mc( tabelaVerdade );
    cout << mc.getExpressaoFinal() << endl;

    return 0;
}