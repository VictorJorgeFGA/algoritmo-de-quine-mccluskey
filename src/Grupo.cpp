#include "Grupo.hpp"
#include <iostream>

Grupo::Grupo(){
    nextIndex = 0;
}

Grupo::Grupo( const Termo & termo ){
    termos.push_back( termo );
    nextIndex = 0;
}

Grupo::Grupo( const vector<Termo> & termos ){
    this->termos = termos;
    nextIndex = 0;
}

void Grupo::clear(){
    termos.clear();
}

void Grupo::add( const Termo & termo ){
    termos.push_back(termo);
}

bool Grupo::hasNextTermo(){
    if( (unsigned long) nextIndex == termos.size() ){
        nextIndex = 0;
        return false;
    }
    return true;
}

int Grupo::size() const{
    return termos.size();
}

Termo & Grupo::proximoTermo(){
    int tmp = nextIndex;
    nextIndex++;
    return termos[ tmp ];
}

Termo & Grupo::termoAt( int index ){
    return termos[ index ];
}

Termo Grupo::termoAt( int index ) const{
    return termos[ index ];
}

Termo & Grupo::operator[]( int index ){
    return termos[ index ];
}

Grupo Grupo::operator*( Grupo & grupo2 ){
    if( size() == 0 || grupo2.size() == 0 )
        return Grupo();

    vector<Termo> newTermos;
    for( int i = 0 ; i < size() ; i++ ){
        for( int j = 0 ; j < grupo2.size() ; j++ ){
            if( termoAt(i).combinaCom( grupo2.termoAt(j) ) ){
                newTermos.push_back( termoAt(i) % grupo2.termoAt(j) );
                termoAt(i).marcar();
                grupo2.termoAt(j).marcar();
            }
        }
    }
    return Grupo( newTermos );
}

Grupo & Grupo::operator=( const Grupo & grupo2 ){
    termos.clear();
    for( int i = 0 ; i < grupo2.size() ; i++ )
        termos.push_back( grupo2.termoAt(i) );
    nextIndex = 0;
    return *(this);
}

void Grupo::exibirGrupo(){
    for( int i = 0 ; (unsigned long) i < termos.size() ; i++ ){
        termos[i].exibir();
        cout << endl;
    }
}