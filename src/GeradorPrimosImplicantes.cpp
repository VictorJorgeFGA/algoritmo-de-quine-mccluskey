#include "GeradorPrimosImplicantes.hpp"
#include <iostream>
#include <set>

GeradorPrimosImplicantes::GeradorPrimosImplicantes( const vector<vector<int>> & tabelaVerdade ){
    gerar( tabelaVerdade );
}

void GeradorPrimosImplicantes::gerarGrupoInicial( const vector<vector<int>> & tabelaVerdade ){
    grupoAtual.clear();
    grupoAtual.resize( tabelaVerdade[0].size() );
    for( int i = 0 ; (unsigned long) i < grupoAtual.size() ; i++ )
        grupoAtual[i] = Grupo();

    for( int linhaAtual = 0 ; (unsigned long) linhaAtual < tabelaVerdade.size() ; linhaAtual++ ){

        int mintermo = tabelaVerdade[ linhaAtual ].at(0);
        mintermos.push_back(mintermo);
        vector<int8_t> bits;

        for( int col = 1 ; (unsigned long) col < tabelaVerdade[ linhaAtual ].size() ; col++ )
            bits.push_back( tabelaVerdade[ linhaAtual ].at(col) );
        
        Termo tmp( mintermo , bits );
        grupoAtual[ tmp.aQueGrupoPertence() ].add(tmp);
    }
}

void GeradorPrimosImplicantes::extrairPrimosImplicantes( int i ){
    //cout << "Para " << i << " ums, temos " << grupoAtual[i].size() << " termos" << endl; 
    for( int j = 0 ; j < grupoAtual[i].size() ; j++ ){
        if( grupoAtual[i].termoAt(j).estaMarcado() == false ){
            //cout << "Estou inserindo o termo ";
            //grupoAtual[i].termoAt(j).exibir();
            //cout << " pois ele nao esta marcado" << endl;
            primosImplicantes.push_back( grupoAtual[i].termoAt(j) );
        }
        // else{
        //     cout << "O termo ";
        //     grupoAtual[i].termoAt(j).exibir();
        //     cout << " esta marcado: " << grupoAtual[i].termoAt(j).estaMarcado() << endl;
        // }
    }
}

void GeradorPrimosImplicantes::gerar( const vector<vector<int>> & tabelaVerdade ){
    gerarGrupoInicial( tabelaVerdade );

    cout << "Grupo inicial: " << endl;
    exibirGrupoAtual();
    cout << endl;

    int contador = 0;
    while(1){
        contador++;
        cout << "Iteracao #" << contador << ":" << endl;
        exibirGrupoAtual();
        vector<Grupo> grupoTemporario( grupoAtual.size() );

        bool novosGruposForamFormados = false;
        for( int i = 0 ; (unsigned long) i < grupoAtual.size() - 1 ; i++ ){
            if( grupoAtual[i].size() == 0 || grupoAtual[i+1].size() == 0 ){
                grupoTemporario[i] = Grupo();
                continue;
            }
            grupoTemporario[i] = grupoAtual[i] * grupoAtual[i+1];

            if( grupoTemporario[i].size() != 0 )
                novosGruposForamFormados = true;
        }
        grupoTemporario[grupoAtual.size()-1] = Grupo();
        cout << "Grupo temporario:" << endl;
        for( int i = 0 ; (unsigned long) i < grupoTemporario.size() ; i++ ){
            grupoTemporario[i].exibirGrupo();
            cout << endl;
        }

        cout << "Fim da iteracao #" << contador << ":" << endl;
        exibirGrupoAtual();

        for( int i = 0 ; (unsigned long) i < grupoAtual.size() ; i++ )
            extrairPrimosImplicantes(i);

        grupoAtual = grupoTemporario;

        if( not novosGruposForamFormados ) break;
    }
}

int GeradorPrimosImplicantes::gruposNaoVazios(){
    int qntde = 0;
    for( int i = 0 ; (unsigned long) i < grupoAtual.size() ; i++ )
        if( grupoAtual.at(i).size() != 0 )
            qntde++;

    return qntde;
}

void GeradorPrimosImplicantes::exibirGrupoAtual(){
    cout << "Grupo Atual: " << endl;
    for( int i = 0 ; (unsigned long) i < grupoAtual.size() ; i++ ){
        grupoAtual[i].exibirGrupo();
        if(grupoAtual[i].size() != 0 ) cout << endl;
    }
    cout << "Size of grupoAtual is:" << grupoAtual.size() << endl;
}

void GeradorPrimosImplicantes::exibirPrimosImplicantes(){
    cout << "Primos implicantes: " << endl;
    for( int i = 0 ; (unsigned long) i < primosImplicantes.size() ; i++ ){
        primosImplicantes[i].exibir();
        cout << endl;
    }
}

const vector<Termo> & GeradorPrimosImplicantes::getPrimosImplicantes() const{
    return primosImplicantes;
}

const vector<int> & GeradorPrimosImplicantes::getMintermos() const{
    return mintermos;
}