#include "Termo.hpp"
#include <iostream>
#include <cstdio>

Termo::Termo( const vector<int> & mintermoIndex , const vector<int8_t> & bits ){
    this->mintermoIndex = mintermoIndex;
    this->bits = bits;
    mark = false;
}

Termo::Termo( int mintermo , const vector<int8_t> & bits ){
    this->bits = bits;
    mintermoIndex.push_back(mintermo);
    mark = false;
}

int Termo::aQueGrupoPertence() const{
    int contador = 0;
    for( int i = 0 ; (unsigned long) i < bits.size() ; i++ )
        if( bits[i] == 1 )
            contador++;

    return contador;
}

int Termo::sizeBits() const{
    return bits.size();
}

int Termo::sizeMintermo() const{
    return mintermoIndex.size();
}

int Termo::getMaiorMintermo() const{
    int maior = -1;
    for( int i = 0 ; i < sizeMintermo() ; i++){
        if( mintermoAt(i) > maior )
            maior = mintermoAt(i);
    }
    return maior;
}

void Termo::set( int index ){
    bits[ index ] = 1;
}

void Termo::flip( int index ){
    bits[ index ] = 0;
}

void Termo::erase( int index ){
    bits[ index ] = -1;
}

void Termo::marcar(){
    mark = true;
}

void Termo::desmarcar(){
    mark = false;
}

int8_t Termo::bitAt( int index ) const{
    return bits[ index ];
}

int Termo::mintermoAt( int index ) const{
    return mintermoIndex[ index ];
}

bool Termo::combinaCom( const Termo & termo2 ) const{
    if( sizeBits() != termo2.sizeBits() )
        return false;

    int diferencas = 0;
    for( int i = 0 ; i < sizeBits() ; i++ ){
        if( bitAt(i) != termo2.bitAt(i) )
            diferencas++;
        if( diferencas > 1 )
            return false;
    }

    return true;
}

bool Termo::estaMarcado() const{
    return mark;
}

bool Termo::cobreMintermo( int mintermo ) const{
    for( int i = 0 ; i < sizeMintermo() ; i++ ){
        if( mintermoAt(i) == mintermo )
            return true;
    }
    return false;
}

Termo Termo::operator%( Termo & termo2 ){
    vector<int8_t> newBits;
    vector<int> newMintermoIndex;
    for( int i = 0 ; i < sizeBits() ; i++ ){
        if( bitAt(i) != termo2.bitAt(i) )
            newBits.push_back( 2 );
        else
            newBits.push_back( bitAt(i) );
    }

    for( int i = 0 ; i < sizeMintermo() ; i++ )
        newMintermoIndex.push_back( mintermoAt(i) );

    for( int i = 0 ; i < termo2.sizeMintermo() ; i++ )
        newMintermoIndex.push_back( termo2.mintermoAt(i) );

    return Termo( newMintermoIndex , newBits );
}


Termo & Termo::operator=( const Termo & termo2 ){
    mintermoIndex.clear();
    bits.clear();

    for( int i = 0 ; i < sizeMintermo() ; i++ )
        mintermoIndex.push_back( termo2.mintermoAt(i) );

    for( int i = 0 ; i < sizeBits() ; i++ )
        bits.push_back( termo2.bitAt(i) );

    return *(this);
}

bool Termo::operator==( const Termo & termo2 ) const{
    if( sizeBits() != termo2.sizeBits() )
        return false;

    for( int i = 0 ; i < sizeBits() ; i++ )
        if( bitAt(i) != termo2.bitAt(i) )
            return false;
    return true;
}

bool Termo::operator<( const Termo & termo2 ) const{
    if( aQueGrupoPertence() < termo2.aQueGrupoPertence() )
        return true;
    return false;
}

bool Termo::operator>( const Termo & termo2 ) const{
    if( aQueGrupoPertence() > termo2.aQueGrupoPertence() )
        return true;
    return false;
}

bool Termo::operator!=( const Termo & termo2 ) const{
    return not operator==(termo2);
}

bool Termo::operator<=( const Termo & termo2 ) const{
    return operator==(termo2) or operator<(termo2);
}

bool Termo::operator>=( const Termo & termo2 ) const{
    return operator==(termo2) or operator>( termo2 );
}

string Termo::toString() const{
    string toReturn;
    for( int i = 0 ; i < sizeBits() ; i++ ){
        if( bitAt(i) == 1 )
            toReturn.push_back( (char) (i + 65) );
        else if( bitAt(i) == 0 ){
            toReturn.push_back( (char) (i + 65) );
            toReturn.push_back('\'');
        }
    }
    return toReturn;
}

void Termo::exibir(){
    for( int i = 0 ; i < sizeMintermo() ; i++ )
        cout << mintermoAt(i) << ", ";
    for( int i = 0 ; i < sizeBits() ; i++ )
        printf("%hhd ", bitAt(i));

    if( estaMarcado() )   cout << "x";
    else cout << ".";
}