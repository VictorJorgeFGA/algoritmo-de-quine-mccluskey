#include "mccluskey.hpp"
#include <iostream>

McCluskey::McCluskey( std::vector<std::vector<int>> tabelaVerdade ){
    this->tabelaVerdade = tabelaVerdade;
    gerarExpressaoFinal();
}

void McCluskey::podarTabelaVerdade(){
    std::vector<std::vector<int>> novaTabelaVerdade;

    for( int linha = 0 ; (unsigned long) linha < tabelaVerdade.size() ; linha++ ){
        if( tabelaVerdade[linha].back() == 1 ){
            std::vector<int> novaLinha;
            novaLinha.push_back( linha );

            for( int col = 0 ; (unsigned long) col < tabelaVerdade[linha].size() - 1 ; col++ )
                novaLinha.push_back( tabelaVerdade[linha][col] );
            
            novaTabelaVerdade.push_back( novaLinha );
        }
    }
    tabelaVerdade.swap( novaTabelaVerdade );
}

void McCluskey::gerarString( const vector<Termo> & lista ){
    expressaoFinal = "";
    expressaoFinal += lista.front().toString();
    for( int i = 1 ; (unsigned long) i < lista.size() ; i++ ){
        expressaoFinal += " + " + lista[i].toString();
    }
}

void McCluskey::gerarExpressaoFinal(){
    podarTabelaVerdade();
    GeradorPrimosImplicantes gp(tabelaVerdade);
    Simplificador s( gp.getPrimosImplicantes() , gp.getMintermos() );
    gerarString( s.getPrimosImplicantes() );
}

std::string McCluskey::getExpressaoFinal(){
    return expressaoFinal;
}

std::vector<std::vector<int>> McCluskey::getTabelaVerdade(){
    return tabelaVerdade;
}

void McCluskey::exibirTabelaVerdade(){
    for( int i = 0 ; (unsigned long) i < tabelaVerdade.size() ; i++ ){
        for( int j = 0 ; (unsigned long) j < tabelaVerdade[i].size() ; j++ )
            std::cout << tabelaVerdade[i][j] << " ";
        std::cout << std::endl;
    }
}