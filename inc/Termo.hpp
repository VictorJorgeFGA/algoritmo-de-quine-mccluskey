#ifndef _TERMO_HPP_
#define _TERMO_HPP_

#include <vector>
#include <string>

using namespace std;

class Termo{
private:
    vector<int> mintermoIndex;
    vector<int8_t> bits;
    bool mark;

public:
    Termo( const vector<int> & mintermoIndex , const vector<int8_t> & bits );
    Termo( int mintermo , const vector<int8_t> & bits );

    int aQueGrupoPertence() const;

    int sizeBits() const;

    int sizeMintermo() const;

    int getMaiorMintermo() const;

    void set( int index );

    void flip( int index );

    void erase( int index );

    void marcar();

    void desmarcar();

    int8_t bitAt( int index ) const;

    int mintermoAt( int index ) const;

    bool combinaCom( const Termo & termo2 ) const;

    bool estaMarcado() const;

    bool cobreMintermo( int mintermo ) const;

    Termo operator%( Termo & termo2 );

    Termo & operator=( const Termo & termo2 );

    bool operator==( const Termo & termo2 ) const;

    bool operator<( const Termo & termo2 ) const;

    bool operator>( const Termo & termo2 ) const;

    bool operator!=( const Termo & termo2 ) const;

    bool operator<=( const Termo & termo2 ) const;

    bool operator>=( const Termo & termo2 ) const;

    string toString() const;

    void exibir();
};

#endif