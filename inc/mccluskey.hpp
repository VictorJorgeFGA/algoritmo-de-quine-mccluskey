#ifndef _MCCLUSKEY_HPP_
#define _MCCLUSKEY_HPP_

#include "GeradorPrimosImplicantes.hpp"
#include "Simplificador.hpp"

#include <string>
#include <vector>

class McCluskey{
private:
    std::string expressaoFinal;
    std::vector<std::vector<int>> tabelaVerdade;

    //Deixa na tabela verdade apenas os mintermos
    //no formato:
    // indice mintermo | bit-1 | bit-2 | ... | bit-n 
    void podarTabelaVerdade();
    void gerarString( const vector<Termo> & lista );

protected:
    void gerarExpressaoFinal();

public:
    McCluskey( std::vector<std::vector<int>> tabelaVerdade );

    std::string getExpressaoFinal();

    std::vector<std::vector<int>> getTabelaVerdade();

    void exibirTabelaVerdade();
};

#endif