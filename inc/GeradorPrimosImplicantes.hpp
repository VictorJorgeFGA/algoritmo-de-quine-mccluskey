#ifndef _GERADORPRIMOSIMPLICANTES_HPP_
#define _GERADORPRIMOSIMPLICANTES_HPP_

#include <vector>
#include <list>
#include "Termo.hpp"
#include "Grupo.hpp"

using namespace std;

class GeradorPrimosImplicantes{
private:
    vector<Termo> primosImplicantes;
    vector<int> mintermos;
    vector<Grupo> grupoAtual;

    void gerarGrupoInicial( const vector<vector<int>> & tabelaVerdade );
    void extrairPrimosImplicantes( int i );

protected:
    void gerar( const vector<vector<int>> & tabelaVerdade );

public:
    GeradorPrimosImplicantes( const vector<vector<int>> & tabelaVerdade );

    int gruposNaoVazios();

    void exibirGrupoAtual();

    void exibirPrimosImplicantes();

    const vector<Termo> & getPrimosImplicantes() const;
    const vector<int> & getMintermos() const;
};

#endif