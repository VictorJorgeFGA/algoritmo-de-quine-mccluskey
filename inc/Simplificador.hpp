#ifndef _SIMPLIFICADOR_HPP_
#define _SIMPLIFICADOR_HPP_

#include <vector>
#include <utility>
#include "GeradorPrimosImplicantes.hpp"

class Simplificador{
    vector<int> mintermos;
    vector<Termo> primos;
    vector<vector<bool>> matriz;
    vector<Termo> primosImplicantes;

    void montarMatriz();
    void simplificar();
    int getTermoEssencial();
    int getTermoRico();
    void cobrirMintermos( int primo );
    

public:
    Simplificador( const vector<Termo> & primos, const vector<int> & mintermos );

    void exibirPrimosImplicantes();

    const vector<Termo> & getPrimosImplicantes() const;
};

#endif