#ifndef _GRUPO_HPP_
#define _GRUPO_HPP_

#include <vector>
#include "Termo.hpp"

using namespace std;

class Grupo{
private:
    vector<Termo> termos;
    int nextIndex;

public:
    Grupo( );
    Grupo( const Termo & termo );
    Grupo( const vector<Termo> & termos );
    
    void clear();

    void add( const Termo & termo );

    bool hasNextTermo();

    int size() const;

    Termo & proximoTermo();

    Termo & termoAt( int index );

    Termo termoAt( int index ) const;

    Termo & operator[]( int index );

    Grupo operator*( Grupo & grupo2 );

    Grupo & operator=( const Grupo & grupo2 );

    void exibirGrupo();
};

#endif